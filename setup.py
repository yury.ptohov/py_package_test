from setuptools import setup, find_packages


setup(
    packages=find_packages(
        where=".",
        include=["inc_dec*"],
        exclude=["tests"],
    ),
    name="inc_dec",
    version="0.1",
    author="yp",
    author_email="yury.ptohov@gmail.com",
    description="Тестирование сборки и деплоя пакета Python",
    license="MIT",
    classifiers=["Python3.5", "GitLab Demo"],
    python_requires=">=3.5",
)