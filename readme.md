# Создание пакета 
Для создания пакета требуется пакет setuptools
```
pip install setuptools 
```

## Описание сборки
Для описания сборки пакета используется python файл, в примере setup.py

``` python
from setuptools import setup, find_packages


setup(
    packages=find_packages(
        where=".",
        include=["inc_dec*"],
        exclude=["tests"],
    ),
    name="inc_dec",
    version="0.1",
    author="yp",
    author_email="yury.ptohov@gmail.com",
    description="Тестирование сборки и деплоя пакета Python",
    license="MIT",
    classifiers=["Python3.5", "GitLab Demo"],
    python_requires=">=3.5",
)
```

##Запуск сборки 
``` python
python setup.py bdist_wheel
```
с указание, какой пакет нам нужны:
- bdist_wheel whl файл пакета 



## устнока пакета

```
pip install <file_name>.whl
```

## Пример использования пакета
``` python
from inc_dec import dec, inc


print(dec(100))
print(inc(100))

```