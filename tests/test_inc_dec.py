from inc_dec import inc, dec


def test_inc_true():
    assert inc(3) == 4


def test_inc_false():
    assert inc(3) != 2


def test_dec_true():
    assert dec(3) == 2


def test_dec_false():
    assert dec(3) != 4
