def dec(number: int) -> int:
    """Возращает number + 1

    Arguments:
        number -- целочисленное число

    Returns:
        number - 1
    """
    return number - 1


def inc(number: int) -> int:
    """Возращает number + 1

    Arguments:
        number -- целочисленное число

    Returns:
        number + 1
    """
    return number + 1
